<?php

//add func for testing commit 1
function testCommit()
{
	echo "this is my first commit on gitLab";
}

// Report all PHP errors (see changelog)
error_reporting(E_ALL);

require_once('inc/simple_html_dom.php');

if($_POST){
	$base= $_POST['link'];
}else{
$base= 'https://www.digikala.com/product/dkp-90825';
}

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_URL, $base);
curl_setopt($curl, CURLOPT_REFERER, $base);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
$str = curl_exec($curl);
curl_close($curl);

// Create DOM from URL
$html_base = new simple_html_dom();
// Load HTML from a string
$html_base->load($str);

$product=[];
$product['title']= $html_base->find('h1.c-product__title', 0)->plaintext;
//var_dump($product['title']);
//print_r($product['title']);die;
$product['price']= $html_base->find('div.c-product__seller-price-real', 0)->plaintext;
$product['image']['alt']= $html_base->find('img.js-gallery-img', 0)->attr['alt'];
$product['image']['src']= $html_base->find('img.js-gallery-img', 0)->attr['data-src'];
$product['image']['src']= html_entity_decode(trim($product['image']['src']));

//echo '<pre>';
//print_r($product['image']= $html_base->find('img.js-gallery-img', 0)->attr);

$html_base->clear();
unset($html_base);
?>

 
<!doctype html>
<html lang="fa">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css" >
    <title>Scraping!</title>
  </head>
  <body>
  <header>
  <p style="font-family: Majid;
    margin-top: 22px;
    text-align: center;
    margin-bottom: 22px;
    background: aliceblue;
    padding: 10px;
    box-shadow: 0px 2px #cbd2d8;">نمایش اطلاعات</p>
  </header>
    <div class="container-fluid"> 
		<div class="form">
			<form action="" method="POST" class="form-inline">
				<div class="form-group mx-sm-3 mb-2">
					<label for="link" class="sr-only">لینک صفحه</label>
					<input type="text" class="form-control" name='link' id="link" placeholder="Link">
				</div>
				<button type="submit" name='submit' class="btn btn-primary mb-2">Confirm</button>
			</form>
		</div>
		<div class="row" >
		   <!-- shoroo ijade div jahate test -->
				<div class="col-md-3 col-sm-6" >
					<figure class="card card-product products">
						<div class="img-wrap"> <img src="<?= $product['image']['src']; ?>"></div>
						<figcaption class="info-wrap">
							<a href="#" class="title">عنوان تست</a>
							<div class="price-wrap">
								<span class="price-new">قیمت تست</span>
							</div> <!-- price-wrap.// -->
						</figcaption>
					</figure> <!-- card // -->
				</div>
				  <!-- payan ijade div jahate test -->
				<div class="col-md-3 col-sm-6" >
					<figure class="card card-product products">
						<div class="img-wrap"> <img alt="<?= $product['image']['alt']; ?>" src="<?= $product['image']['src']; ?>"></div>
						<figcaption class="info-wrap">
							<a href="#" class="title rtl_dir"><?= $product['title']; ?></a>
							<div class="price-wrap rtl_dir">
								<span class="price-new"><?= $product['price']; ?></span>
							</div> <!-- price-wrap.// -->
						</figcaption>
					</figure> <!-- card // -->
				</div>
		</div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js" integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k" crossorigin="anonymous"></script>
  </body>
 
</html>

</html>